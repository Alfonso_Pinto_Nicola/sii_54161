#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main(){
	
	int fd_fifo;
	int fifo;	
	char c;
	
	//Creacion de la tuberia		
	if(fd_fifo=mkfifo("Logger",0660)){
		perror("mkfifo");
	}
	
	//Apertura en modo lectura
	if(fifo=open("Logger",O_RDONLY)){
		perror("open");
	}
	
	//Escritura por la salida estandar
	while(read(fifo,&c,1)>0){
		write(1,&c,1);
	}
	
	close(fifo);
	unlink("Logger");
	return 0;
}
