// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////




CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{
	
	pdatosCompartidos->estado=1;
	
	
	//----Se procede a cerrar las comunicaciones
	munmap(pdatosCompartidos,sizeof(datosCompartidos));
	unlink("datosCompartidos");
	
	
	close(fifo_servidor_cliente);
	unlink("FIFO_SERVIDOR_CLIENTE");
	
	close(fifo_cliente_servidor);
	unlink("FIFO_CLIENTE_SERVIDOR");

}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	esfera.extra();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	char cad[200];
	
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		int aux2;
		aux2=sprintf(cad,"Jugador 2 marca. Jugador 2 lleva %d puntos \n",puntos2);
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		
		int aux1;
		aux1=sprintf(cad,"Jugador 1 marca. Jugador 1 lleva %d puntos \n",puntos1);
		
	}
	
	
	//-----LEEMOS LOS DATOS ENVIADOS POR EL SERVIDOR
	read(fifo_servidor_cliente,cad,sizeof(cad));

	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", 
		&esfera.centro.x,&esfera.centro.y, 
		&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, 
		&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, 
		&puntos1, &puntos2); 
	
	
	//-----Actualizacion de los datos
	pdatosCompartidos->esfera=esfera;
	pdatosCompartidos->raqueta1=jugador1;
	
	//Efectuo los movimientos
	if(pdatosCompartidos->accion==-1){OnKeyboardDown('s', 0, 0);}
	else if(pdatosCompartidos->accion==0){}
	else if(pdatosCompartidos->accion==1){OnKeyboardDown('w', 0, 0);}
	
	if((puntos1==3)||(puntos2==3)){
		puntos1=0;
		puntos2=0;
	}
}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	
	//switch(key){
	
	//Jugador 1
	//case 's':jugador1.velocidad.y=-6;break;
	//case 'w':jugador1.velocidad.y=6;break;
	
	//Jugador 2
	//case 'l':jugador2.velocidad.y=-4;break;
	//case 'o':jugador2.velocidad.y=4;break;
	//}
	
	//----Escribo la tecla
	write(fifo_cliente_servidor,&key,sizeof(key));
}

void CMundoCliente::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;  
	
	//Velocidad de la esfera
	esfera.SetSpd(2,2);
	
	//----CREACION DE LA MEMORIA COMPARTIDA
	int fd_mmap;

	fd_mmap= open("datosCompartidos", O_CREAT|O_TRUNC|O_RDWR, 0666);
	if (fd_mmap < 0) {
           perror("Error creación fichero destino");
           exit(1);
        }
	datosCompartidos.esfera=esfera;
	datosCompartidos.raqueta1=jugador1;
	datosCompartidos.accion=0;
	datosCompartidos.estado=0;
	
	write(fd_mmap,&datosCompartidos,sizeof(datosCompartidos));
	
	pdatosCompartidos=static_cast<DatosMemCompartida*>(mmap(NULL,sizeof(datosCompartidos), PROT_READ|PROT_WRITE, MAP_SHARED, fd_mmap, 0));
	close(fd_mmap);
	
	if (pdatosCompartidos==MAP_FAILED){
		perror("Error en la proyeccion de Datps compartidos");
		return;
	}
	

	//----CREACION Y APERTURA EN MODO LECTURA FIFO SERVIDOR CLIENTE

	mkfifo("FIFO_SERVIDOR_CLIENTE",0600);
	fifo_servidor_cliente=open("FIFO_SERVIDOR_CLIENTE",O_RDONLY);
	if(fifo_servidor_cliente<0){
		perror("Error en la creacion de FIFO_SERVIDOR_CLIENTE");
		return;
	}
	
	//----CREACION Y APERTURA EN MODO LECTURA FIFO CLIENTE SERVIDOR

	mkfifo("FIFO_CLIENTE_SERVIDOR",0600);
	fifo_cliente_servidor=open("FIFO_CLIENTE_SERVIDOR",O_WRONLY);
	if(fifo_cliente_servidor<0){
		perror("Error en la creacion de FIFO_CLIENTE_SERVIDOR");
		return;
	}	
	
	
	
}


