// Raqueta.h: interface for the Raqueta class.
//
//	UPM Sistemas informaticos industriales
//	Desarrollador: Alfonso Pinto Nicola
//////////////////////////////////////////////////////////////////////

#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	//Vector2D posicion;
	Vector2D velocidad;
	Vector2D centro=getCentro();

	Raqueta();
	virtual ~Raqueta();

	//void SetPos(float posx,float posy){posicion.x=posx;posicion.y=posy;};
	void SetSpd(float spdx,float spdy){velocidad.x=spdx;velocidad.y=spdy;};
	void Mueve(float t);
	
}; 
